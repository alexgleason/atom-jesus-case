toJesusCase = require('to-jesus-case')

module.exports =
  activate: ->
    atom.commands.add 'atom-workspace', 'editor:jesus-case': => @convert()

  convert: ->
    editor = atom.workspace.getActiveTextEditor()
    buffer = editor.getBuffer()
    selections = editor.getSelections()

    # Group these actions so they can be undone together
    buffer.transact ->
      for selection in selections
        cased = toJesusCase selection.getText()
        selection.insertText("#{cased}")
